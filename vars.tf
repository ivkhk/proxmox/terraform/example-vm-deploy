# CHANGE IT!
variable "api_url" {
  type    = string
  default = "https://proxmox.local:8006/api2/json"
}

variable "token_id" {
  type    = string
  default = "user@pve!proxmox"
}

variable "token_secret" {
  type    = string
  default = "abcd11111-1111-11a1-1a11-111a1111bcd1"
}

variable "node" {
  type    = string
  default = "pve-1"
}

variable "template_name" {
  type    = string
  default = "VM-Template"
}

variable "vm_name" {
  type    = string
  default = "VM-Terraform"
}

variable "pool" {
  type    = string
  default = "vm_pool"
}

variable "network_bridge" {
  type    = string
  default = "vmbr0"
}

variable "storage" {
  type    = string
  default = "local-lvm"
}

variable "cores" {
  type    = number
  default = 2
}

variable "memory" {
  type    = number
  default = 2048
}

variable "size" {
  type    = number
  default = 8
}
