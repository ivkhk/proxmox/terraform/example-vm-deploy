resource "proxmox_vm_qemu" "terraform" {
  name         = var.vm_name
  force_create = false
  agent        = 1
  cores        = var.cores
  memory       = var.memory
  boot         = "order=scsi0"
  target_node  = var.node
  pool         = var.pool
  clone        = var.template_name
  full_clone   = true
  scsihw       = "virtio-scsi-single"
  disks {
    scsi {
        scsi0 {
            disk {
                storage = var.storage
                size    = var.size
            }
        }
    }
  }
  network {
    bridge = var.network_bridge
    model  = "virtio"
  }
}
